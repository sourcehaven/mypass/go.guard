package ini

import "C"
import (
	"github.com/sirupsen/logrus"
	"gitlab.com/sourcehaven/mypass/go.guard/pkg/app"
	"gitlab.com/sourcehaven/mypass/go.guard/pkg/models"
)

func dummyDbInit() {
	err := app.DB.AutoMigrate(&models.User{})
	if err != nil {
		logrus.Fatal(err)
	}
	app.DB.Create(&models.User{
		Username:  "dummy",
		Email:     "dummy@mail.com",
		Password:  "foobar",
		Firstname: "foo",
		Lastname:  "bar",
	})
}
