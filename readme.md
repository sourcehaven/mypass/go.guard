# MyPass Go Database Bridge

An application to serve as a bridge between **MyPass** clients and the database.

## Development

Generate swagger (OpenAPI) documentation

> swag init --dir=./cmd/mypass

> swag init --generalInfo=./cmd/mypass/main.go

### All-round development

Here is an all around tutorial on writing fiber app with go.
Follow this [link](https://dev.to/koddr/build-a-restful-api-on-go-fiber-postgresql-jwt-and-swagger-docs-in-isolated-docker-containers-475j).

## Hashing password

Great tutorial on hashing passwords with argon,
[here](https://www.alexedwards.net/blog/how-to-hash-and-verify-passwords-with-argon2-in-go).
